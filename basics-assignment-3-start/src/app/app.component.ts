import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  styles: [`
    .textColor {
      color: white;
    }
  `]

})
export class AppComponent {
  buttonClicked = false;
  clicks = [''];
  onButtonClick() {
    this.buttonClicked = true;
    this.clicks.push(new Date().toLocaleString());
  }
}
